import org.gradle.api.Project
import org.gradle.kotlin.dsl.repositories

fun Project.setupRepositories(){
    repositories {
        jcenter()
        mavenCentral()
    }
}