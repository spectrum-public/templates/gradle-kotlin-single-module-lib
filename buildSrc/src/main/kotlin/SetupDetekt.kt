import org.gradle.api.Project
import org.gradle.kotlin.dsl.*

fun Project.setupDetekt(){

    buildscript {
        repositories {
            maven {
                url = uri("https://plugins.gradle.org/m2/")
            }
        }
        dependencies {
            classpath("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.0.0-RC14")
        }
    }

    apply(plugin = "io.gitlab.arturbosch.detekt")
}