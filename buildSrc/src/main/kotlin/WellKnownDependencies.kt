import org.gradle.kotlin.dsl.DependencyHandlerScope

fun DependencyHandlerScope.coroutines(version: String = "1.2.0") {
    add("compile", "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version")
}

fun DependencyHandlerScope.logback(version: String = "1.2.3") {
    add("compile", "ch.qos.logback:logback-classic:$version")
}

fun DependencyHandlerScope.gson(version: String = "2.8.5") {
    add("compile", "com.google.code.gson:gson:$version")
}

