import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.withType

fun Project.setupTests() {
    dependencies {
        add("testImplementation", "io.kotlintest:kotlintest-runner-junit5:3.3.2")
    }
    tasks.withType<Test> {
        @Suppress("UnstableApiUsage")
        useJUnitPlatform{

        }
    }
}