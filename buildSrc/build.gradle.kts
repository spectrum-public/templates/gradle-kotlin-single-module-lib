plugins {
    `kotlin-dsl`
}


repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.30")
}