import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec

class HelloWorldTest : StringSpec( {
    "hello is world" { HelloWorld().hello() shouldBe "world" }
})