import groovy.lang.GroovyObject
import org.jfrog.gradle.plugin.artifactory.dsl.DoubleDelegateWrapper
import org.jfrog.gradle.plugin.artifactory.dsl.PublisherConfig

plugins {
    id("io.gitlab.arturbosch.detekt") version "1.0.0-RC14"
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.4"
    id("com.jfrog.artifactory") version "4.9.0"
}
setupRepositories()
setupKotlin()
setupTests()
dependencies{
    //coroutines()
    //logback()
    //gson()
}

version = "1.0.0-SNAPSHOT"
group = "codes.spectrum.public"
val self = this
publishing{
    publications {
        create<MavenPublication>("maven") {
            groupId = self.group.toString()
            artifactId = self.name
            version = self.version.toString()
            from(components["java"])
        }
    }
}

if(tasks.findByName("deploy")==null){
    tasks.register("deploy")
}
artifactory {
    setContextUrl("https://oss.jfrog.org/artifactory")
    //The base Artifactory URL if not overridden by the publisher/resolver
    publish(delegateClosureOf<PublisherConfig> {
        repository(delegateClosureOf<DoubleDelegateWrapper> {
            invokeMethod("setRepoKey", "oss-snapshot-local")
            invokeMethod("setUsername", System.getenv("BINTRAY_USER"))
            invokeMethod("setPassword", System.getenv("BINTRAY_PASSWORD"))
            invokeMethod("setMavenCompatible", true)
            invokeMethod("setPublishBuildInfo", false)
        })

        defaults(delegateClosureOf<GroovyObject> {
            invokeMethod("publications", "maven")
        })
    })


}


tasks{

    val check by existing
    val detekt by existing
    val deploy by existing
    val publish by existing
    publish {
        dependsOn(artifactoryDeploy)
        dependsOn(artifactoryPublish)
    }

    if(ensureProperty("with-detekt",false)) {
        check {
            dependsOn(detekt)
        }
    }

    if(ensureProperty("publish",false)) {
        deploy {
           // dependsOn(publish)
        }
    }
}